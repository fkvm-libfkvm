/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define FKVM_INTERNAL
#include <sys/fkvm.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>

#include "libkvm.h"
#include "libfkvm-common.h"
#include "disasm.h"

#define reg_size() _reg_size(regs, sregs)
#define test_repeat_noop() _test_repeat_noop(rep && string, \
					     reg_size(), regs)
#define test_repeat_tail() _test_repeat_tail(rep && string, \
					     reg_size(), regs)

/* TODO: check return value for kvm->callbacks->in* */
static int
emulate_ioio_in(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint16_t port,
	     size_t size,
	     bool rep,
	     bool string)
{
	uint32_t data = 0;

	if (test_repeat_noop())
		return 0;

	do {
		switch (size) {
			case 1:
				kvm->callbacks->inb(kvm->opaque,
						    port,
						    (uint8_t*)  &data);
				break;
			case 2:
				kvm->callbacks->inw(kvm->opaque,
						    port,
						    (uint16_t*) &data);
				break;
			case 4:
				kvm->callbacks->inl(kvm->opaque,
						    port,
						    (uint32_t*) &data);
				break;
		}
		if (!string) {
			regs->rax = data;
		}
		else {
			regs->rdi = mask_reg(regs->rdi, reg_size());

			/* TODO: es limit check */
			cpu_virtual_memory_rw(sregs->es.base + regs->rdi,
					      (uint8_t*) &data,
					       size,
					       1);

			if ((regs->rflags & RFLAGS_DF_MASK) != 0)
				regs->rdi -= size;
			else
				regs->rdi += size;
		}
	} while (test_repeat_tail());

	return 0;
}

/* TODO: check return value for kvm->callbacks->in* */
static int
emulate_ioio_out(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint16_t port,
	     size_t size,
	     bool rep,
	     bool string)
{
	uint32_t data = 0;

	if (test_repeat_noop())
		return 0;

	do {
		if (!string) {
			data = regs->rax;
		}
		else {
			/* SVM won't tell us whether there's a SEG prefix,
			 * so we need to decode the instruction */
			struct kvm_segment *segment = &sregs->ds;
			x86_insn_t insn;
			unsigned int insn_size;

			libdisasm_init(reg_size());
			insn_size = get_x86_insn(sregs->cs.base + regs->rip, &insn);
			if (insn_size == -1)
				EXIT_ERR_PATH();
			segment = kvm_seg_from_x86_op(sregs, x86_operand_2nd(&insn));
			libdisasm_cleanup();

			regs->rsi = mask_reg(regs->rsi, reg_size());

			/* TODO: segment limit check */
			cpu_virtual_memory_rw(segment->base + regs->rsi,
					      (uint8_t*) &data,
					      size,
					      0);

			if ((regs->rflags & RFLAGS_DF_MASK) != 0)
				regs->rsi -= size;
			else
				regs->rsi += size;
		}
		switch (size) {
			case 1:
				kvm->callbacks->outb(kvm->opaque,
						     port,
						     (uint8_t) data);
				break;
			case 2:
				kvm->callbacks->outw(kvm->opaque,
						     port,
						     (uint16_t) data);
				break;
			case 4:
				kvm->callbacks->outl(kvm->opaque,
						     port,
						     (uint32_t) data);
				break;
		}
	} while (test_repeat_tail());

	return 0;
}

int
emulate_ioio(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint16_t port,
	     size_t size,
	     bool rep,
	     bool string,
	     bool in)
{
	switch (size) {
		case 1:
		case 2:
		case 4:
			break;
		default:
			printf("emulate_ioio: invalid size (%u)\n", (int) size);
			EXIT_ERR_PATH();
			return -1;
	}

	if (in) {
		return emulate_ioio_in(kvm, regs, sregs,
				       port, size, rep, string);
	}
	else {
		return emulate_ioio_out(kvm, regs, sregs,
					port, size, rep, string);
	}
}
