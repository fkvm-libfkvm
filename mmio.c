/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define FKVM_INTERNAL
#include <sys/fkvm.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <assert.h>
#include <libdis.h>

#include "libkvm.h"
#include "libfkvm-common.h"
#include "disasm.h"

#define reg_size() _reg_size(regs, sregs)
#define test_repeat_noop() _test_repeat_noop(get_repeat_prefix(insn) == insn_rep_zero, \
					     reg_size(), regs)
#define test_repeat_tail() _test_repeat_tail(get_repeat_prefix(insn) == insn_rep_zero, \
					     reg_size(), regs)

static int
cb_mmio_read(kvm_context_t kvm,
	     uint64_t addr,
	     uint8_t *data,
	     int len)
{
	int error;
	if ((addr & 0xFFF) + len - 1 > 0xFFF) {
		printf("mmio_read over page boundary\n");
		EXIT();
	}
	error = kvm->callbacks->mmio_read(kvm->opaque, addr,
					  data, len);
	return error;
}

static int
cb_mmio_write(kvm_context_t kvm,
	      uint64_t addr,
	      uint8_t *data,
	      int len)
{
	int error;
	if ((addr & 0xFFF) + len - 1 > 0xFFF) {
		printf("mmio_write over page boundary\n");
		EXIT();
	}
	error = kvm->callbacks->mmio_write(kvm->opaque, addr,
					   data, len);
	return error;
}

static int
emulate_mmio_cmp(kvm_context_t kvm,
		 struct kvm_regs *regs,
		 struct kvm_sregs *sregs,
		 uint64_t fault_addr,
		 x86_insn_t *insn)
{
	uint64_t source;
	uint64_t dest;
	uint64_t result;
	uint64_t sign_mask;
	uint8_t SF, PF, CF, OF, AF, ZF;
	uint8_t data[8];
	//int error;
	int i;
	unsigned int dest_size;
	unsigned int src_size;
	x86_op_t * dest_op;
	x86_op_t *src_op;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 2);

	dest_op = x86_operand_1st(insn);
	src_op  = x86_operand_2nd(insn);

	dest_size = x86_operand_size(dest_op);
	src_size  = x86_operand_size(src_op);

	assert(dest_size == src_size);

	switch (src_op->type) { /* source type */
		case op_immediate:
			if (dest_op->type != op_expression)
				EXIT();

			if ((dest_op->flags & ~0xFF) == 0)
				dest_op->flags |= op_ds_seg;

			source = get_source_data(regs, src_op);

			dest = get_memi_address(regs, sregs, dest_op, reg_size());
			//printf("Destination Address: %" PRIx64 "\n", dest);
			dest = kvm_get_phys_addr(dest);
			if (dest != fault_addr) {
				EXIT_ERR_PATH();
			}
			
			memset(data, 0, sizeof(data));
			cb_mmio_read(kvm, dest, data, dest_size);
			dest = *(uint64_t *)data;

			break;

		default:
			printf("op: %d\n", src_op->type);
			EXIT();
	}
	/* CMP emulation */

#define SIGN_MASK_64 0x8000000000000000
#define SIGN_MASK_32 0x80000000
#define SIGN_MASK_16 0x8000
#define SIGN_MASK_8  0x80

	switch(dest_size) {
		case (1) : sign_mask = SIGN_MASK_8;
			   result = (uint8_t)dest - (uint8_t)source;
			   break;
		case (4) : sign_mask = SIGN_MASK_32;
			   result = (uint32_t)dest - (uint32_t)source;
			   break;
		case (8) : sign_mask = SIGN_MASK_64;
			   result = dest - source;
			   break;
		case (2) :
		default  : sign_mask = SIGN_MASK_16;
			   result = (uint16_t)dest - (uint16_t)source;
			   break;
	}

	//printf("The dest is %" PRIx64 "\n", dest);
	//printf("The src is  %" PRIx64 "\n", source);
	CF = (dest < source);
	ZF = (dest == source);
	SF = (result & sign_mask);
	if ((dest & sign_mask) == 0 && (source & sign_mask) != 0)
		OF = SF;
	else if ((dest & sign_mask) != 0 && (source & sign_mask) == 0)
		OF = !SF;
	else
		OF = 0;
#if 0
	/* Taken from AMD VOL 3, CMP instruction information */
	if((int64_t)dest > (int64_t)source)
		OF = SF;
	else if ((int64_t)dest < (int64_t)source)
		OF = !SF;
	else
		OF = 0;
#endif
	PF = 1;
	for (i=0; i < 8; i++) {
		PF = PF ^ ((result >> i) & 1);
	}

	AF = ( (dest & 0x0F) >= (source & 0x0F) );

	regs->rflags &= ~(CF_MASK | PF_MASK | AF_MASK | ZF_MASK | SF_MASK | OF_MASK);
	regs->rflags |= (CF) ? CF_MASK : 0;
	regs->rflags |= (PF) ? PF_MASK : 0;
	regs->rflags |= (AF) ? AF_MASK : 0;
	regs->rflags |= (ZF) ? ZF_MASK : 0;
	regs->rflags |= (SF) ? SF_MASK : 0;
	regs->rflags |= (OF) ? OF_MASK : 0;

//	EXIT();
	return 0;
}

static int
emulate_mmio_movs(kvm_context_t kvm,
		  struct kvm_regs *regs,
		  struct kvm_sregs *sregs,
		  uint64_t fault_addr,
		  x86_insn_t *insn)
{
		uint64_t dest_operand, src_operand;
		uint8_t data[8];
		bool mmio_write;
		bool mmio_read;
		int error = 0;
		int reg_idx1;
		int reg_idx2;
		unsigned int dest_size;
		unsigned int src_size;
		x86_reg_t *dest;
		x86_reg_t *src;
		x86_op_t *dest_op;
		x86_op_t *src_op;

		dest_op = x86_operand_1st(insn);
		src_op  = x86_operand_2nd(insn);

		assert(get_repeat_prefix(insn) == insn_no_prefix ||
		       get_repeat_prefix(insn) == insn_rep_zero);
		assert(insn->explicit_count == 2);

		assert(dest_op->data.expression.index.id == 0);
		assert(src_op->data.expression.index.id == 0);

		assert(dest_op->type == op_expression);
		assert(src_op->type == op_expression);

		dest = &dest_op->data.expression.base;
		src = &src_op->data.expression.base;

		reg_idx1 = kvm_reg_from_x86_reg(src->id);
		reg_idx2 = kvm_reg_from_x86_reg(dest->id);

		assert(reg_idx1 == KVM_REG_RSI);
		assert(reg_idx2 == KVM_REG_RDI);
		assert((dest_op->flags & op_es_seg) == op_es_seg);
		assert((src_op->flags & op_ds_seg) == op_ds_seg);

		dest_size = x86_operand_size(dest_op);
		src_size = x86_operand_size(src_op);

		assert(dest_size == src_size);

		/* TODO: Can it ever be a segment other than DS 
		if (inst->operands[0].memi.base.segment == SEGMENT_DEFAULT)
			inst->operands[0].memi.base.segment = SEGMENT_DS;
		*/

		if (test_repeat_noop())
			return 0;

		dest_operand = get_memi_address(regs, sregs, dest_op, reg_size());
		src_operand  = get_memi_address(regs, sregs, src_op,  reg_size());

		//printf("dest_operand: %" PRIx64 "\n", dest_operand);
		//printf("src_operand:  %" PRIx64 "\n", src_operand);

		if (kvm_get_phys_addr(src_operand) == fault_addr) { // Read
			uint64_t dest_phys_addr;
			dest_phys_addr = kvm_get_phys_addr(dest_operand);
			if (dest_phys_addr == -1)
				EXIT_ERR_PATH();
			mmio_read  = true;
			mmio_write = !kvm_is_containing_region(kvm,
						dest_phys_addr, dest_size);
		}
		else if (kvm_get_phys_addr(dest_operand) == fault_addr) { // Write
			uint64_t src_phys_addr;
			src_phys_addr = kvm_get_phys_addr(src_operand);
			if (src_phys_addr == -1)
				EXIT_ERR_PATH();
			mmio_write = true;
			mmio_read  = !kvm_is_containing_region(kvm,
						src_phys_addr, src_size);
		}
		else
			EXIT_ERR_PATH();

		/* TODO: what if we keep on writing past the page boundary due
		 * to REP? */

		do {
			if (mmio_read) {
				uint64_t src_phys_addr;
				src_phys_addr = kvm_get_phys_addr(src_operand);
				if (src_phys_addr == -1)
					EXIT_ERR_PATH();
				error = cb_mmio_read(kvm,
						     src_phys_addr,
						     data,
						     dest_size);
			}
			else {
				cpu_virtual_memory_rw(src_operand, data,
						      dest_size, 0);
			}

			if (mmio_write) {
				uint64_t dest_phys_addr;
				dest_phys_addr = kvm_get_phys_addr(dest_operand);
				if (dest_phys_addr == -1)
					EXIT_ERR_PATH();
				error = cb_mmio_write(kvm,
						      dest_phys_addr,
						      data,
						      dest_size);
			}
			else {
				cpu_virtual_memory_rw(dest_operand, data,
						      dest_size, 1);
			}

			if (error != 0)
				return error;

			if ((regs->rflags & RFLAGS_DF_MASK) != 0) {
				regs->rsi     -= dest_size;
				regs->rdi     -= dest_size;
				src_operand   -= dest_size;
				dest_operand  -= dest_size;
			}
			else {
				regs->rsi     += dest_size;
				regs->rdi     += dest_size;
				src_operand   += dest_size;
				dest_operand  += dest_size;
			}

		} while (test_repeat_tail());

		return 0;
}

static int
emulate_mmio_stos(kvm_context_t kvm,
		  struct kvm_regs *regs,
		  struct kvm_sregs *sregs,
		  uint64_t fault_addr,
		  x86_insn_t *insn)
{
		uint64_t dest_operand;
		uint64_t source;
		uint8_t source_data[8];
		int error;
		int reg_idx1;
		int reg_idx2;
		unsigned int dest_size;
		unsigned int src_size;
		x86_reg_t *dest;
		x86_op_t *dest_op;
		x86_op_t *src_op;

		dest_op = x86_operand_1st(insn);
		src_op  = x86_operand_2nd(insn);

		assert(insn->explicit_count == 2);

		assert(dest_op->type == op_expression);
		assert((dest_op->flags & op_es_seg) == op_es_seg);
		assert(dest_op->data.expression.index.id == 0);
		assert(src_op->type == op_register);

		dest = &dest_op->data.expression.base;

		reg_idx1 = kvm_reg_from_x86_reg(dest->id);
		reg_idx2 = kvm_reg_from_x86_reg(src_op->data.reg.id);
		assert(reg_idx1 == KVM_REG_RDI);
		assert(reg_idx2 == KVM_REG_RAX);

		dest_size = x86_operand_size(dest_op);
		src_size = x86_operand_size(src_op);

		assert(dest_size == src_size);

		assert(get_repeat_prefix(insn) == insn_no_prefix ||
		       get_repeat_prefix(insn) == insn_rep_zero);

		if (test_repeat_noop())
			return 0;

		dest_operand = get_memi_address(regs, sregs, dest_op, reg_size());
		source  = mask_reg(regs->rax, reg_size());

		//printf("dest_operand: %" PRIx64 "\n", dest_operand);
		//printf("source:  %" PRIx64 "\n", source);

		if (kvm_get_phys_addr(dest_operand) != fault_addr)
			EXIT_ERR_PATH();

		/* TODO: what if we keep on writing past the page boundary due
		 * to REP? */

		*(uint64_t*) source_data = source;

		do {

			uint64_t dest_phys_addr;
			dest_phys_addr = kvm_get_phys_addr(dest_operand);
			if (dest_phys_addr == -1)
				EXIT_ERR_PATH();

			error = cb_mmio_write(kvm, dest_phys_addr, source_data, dest_size);
			if (error != 0)
				return error;

			if ((regs->rflags & RFLAGS_DF_MASK) != 0) {
				regs->rdi    -= dest_size;
				dest_operand -= dest_size;
			}
			else {
				regs->rdi    += dest_size;
				dest_operand += dest_size;
			}

		} while (test_repeat_tail());

		return 0;
}

static int
emulate_mmio_mov(kvm_context_t kvm,
		 struct kvm_regs *regs,
		 struct kvm_sregs *sregs,
		 uint64_t fault_addr,
		 x86_insn_t *insn)
{
	uint64_t source;
	uint64_t dest;
	uint8_t data[8];
	int error;
	unsigned int dest_size;
	unsigned int src_size;
	x86_op_t *dest_op;
	x86_op_t *src_op;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 2);

	dest_op = x86_operand_1st(insn);
	src_op  = x86_operand_2nd(insn);

	dest_size = x86_operand_size(dest_op);
	src_size  = x86_operand_size(src_op);

	if (strcmp(insn->mnemonic, "movzx") == 0)
		/* pass */;
	else if (strcmp(insn->mnemonic, "movsx") == 0)
		EXIT();
	else
		assert(dest_size == src_size);

	switch (src_op->type) { /* source type */
		case op_register:
		case op_immediate:
			if ((dest_op->type != op_expression) &&
			    (dest_op->type != op_offset))
				EXIT();

			if ((dest_op->flags & ~0xFF) == 0)
				dest_op->flags |= op_ds_seg;

			*(uint64_t*) data = get_source_data(regs, src_op);

			dest = get_memi_address(regs, sregs, dest_op, reg_size());
			//printf("Destination Address: %" PRIx64 "\n", dest);

			dest = kvm_get_phys_addr(dest);

			if (dest != fault_addr) {
				EXIT_ERR_PATH();
			}

			error = cb_mmio_write(kvm, dest, data, dest_size);
			if (error != 0)
				return error;

			break;

		case op_expression:
		case op_offset:
			if (dest_op->type != op_register)
				EXIT();
			if ((src_op->flags & ~0xFF) == 0)
				src_op->flags |= op_ds_seg;

			source = get_memi_address(regs, sregs, src_op, reg_size());
			printf("source     = %016" PRIx64 " -> %016" PRIx64 "\n",
			       source,
			       kvm_get_phys_addr(source));
			printf("fault_addr = %016" PRIx64 " -> %016" PRIx64 "\n",
			       fault_addr,
			       kvm_get_phys_addr(fault_addr));

			source = kvm_get_phys_addr(source);

			if (source != fault_addr) {
				printf("eax: 0x%" PRIx64 "\n", regs->rax);
				EXIT_ERR_PATH();
			}

			error = cb_mmio_read(kvm, source, data, src_size);
			if (error != 0)
				return error;

			dest = *(uint64_t*) data;
			dest = mask_reg(dest, reg_size());
			kvm_regs_set(regs,
				     kvm_reg_from_x86_reg(dest_op->data.reg.id),
				     dest);

			break;

		default:
			printf("op: %d\n", src_op->type);
			EXIT();
	}

	return 0;
}

static int
emulate_mmio_xchg(kvm_context_t kvm,
		  struct kvm_regs *regs,
		  struct kvm_sregs *sregs,
		  uint64_t fault_addr,
		  x86_insn_t *insn)
{
	x86_op_t *mem_op;
	x86_op_t *reg_op;
	unsigned int mem_size;
	unsigned int reg_size;
	uint64_t reg_data;
	uint64_t mem_addr;
	uint64_t mem_data;
	int error;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 2);

	mem_op = x86_operand_1st(insn);
	reg_op = x86_operand_2nd(insn);

	assert(mem_op->type == op_expression);
	assert(reg_op->type == op_register);

	mem_size = x86_operand_size(mem_op);
	reg_size = x86_operand_size(reg_op);
	assert(reg_size == mem_size);

	/* read data */
	reg_data = get_source_data(regs, reg_op);

	if ((mem_op->flags & ~0xFF) == 0)
		mem_op->flags |= op_ds_seg;
	mem_addr = get_memi_address(regs, sregs, mem_op, reg_size());
	mem_addr = kvm_get_phys_addr(mem_addr);
	if (mem_addr != fault_addr)
		EXIT_ERR_PATH();
	error = cb_mmio_read(kvm, mem_addr, (uint8_t*) &mem_data, mem_size);
	if (error != 0)
		EXIT_ERR_PATH();

	/* exchange */
	{
		uint64_t tmp_data;
		tmp_data = mem_data;
		mem_data = reg_data;
		reg_data = tmp_data;
	}

	/* write data */
	kvm_regs_set(regs,
		     kvm_reg_from_x86_reg(reg_op->data.reg.id),
		     reg_data);
	error = cb_mmio_write(kvm, mem_addr, (uint8_t*) &mem_data, mem_size);
	if (error != 0)
		EXIT_ERR_PATH();

	return 0;

}

int
emulate_mmio(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint64_t fault_addr)
{
	x86_insn_t insn;
	uint64_t loc, next_rip;
	unsigned int insn_size;
	int error = 0;
	//x86_op_t *op;

	libdisasm_init(reg_size());

	loc = sregs->cs.base + regs->rip;
	insn_size = get_x86_insn(loc, &insn);
	if (insn_size == -1)
		EXIT_ERR_PATH();

	//printf("rip: %" PRIx64 "\n", regs->rip);
	//printf("loc: %" PRIx64 "\n", loc);
	next_rip = regs->rip + insn_size;


	//printf("Number of args: %d\n", (int)insn.explicit_count);

	#if 0
	op = x86_operand_1st(&insn);
	printf("Dest Type = %d\n", op->type);
	op = x86_operand_2nd(&insn);
	printf("Src Type = %d\n", op->type);
	op = x86_operand_3rd(&insn);
	if (op != NULL)
		printf("Type = %d\n", op->type);
	else
		printf("Op was null\n");
	#endif

	switch (insn.type) {

	case insn_strmov:
		error = emulate_mmio_movs(kvm, regs, sregs, fault_addr, &insn);
		break;
	case insn_strstore:
		error = emulate_mmio_stos(kvm, regs, sregs, fault_addr, &insn);
		break;
	case insn_mov:
		error = emulate_mmio_mov(kvm, regs, sregs, fault_addr, &insn);
		break;
	case insn_cmp:
		error = emulate_mmio_cmp(kvm, regs, sregs, fault_addr, &insn);
		break;
	case insn_xchg:
		error = emulate_mmio_xchg(kvm, regs, sregs, fault_addr, &insn);
		break;
	default:
		printf("MMIO %s insn.type=%d\n", insn.mnemonic, insn.type);
		EXIT();
	}

	if (error == 0)
		regs->rip = next_rip;

	libdisasm_cleanup();

	return error;
}
