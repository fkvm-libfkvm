/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define FKVM_INTERNAL
#include <sys/fkvm.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <assert.h>
#include <libdis.h>

#include "libkvm.h"
#include "libfkvm-common.h"
#include "disasm.h"

#define reg_size() _reg_size(regs, sregs)

static int
emulate_cr_mov(kvm_context_t kvm,
	       struct kvm_regs *regs,
	       struct kvm_sregs *sregs,
	       int cr_num,
	       bool is_write,
	       x86_insn_t *insn)
{
	uint64_t data;
	unsigned int dest_size;
	unsigned int src_size;
	x86_op_t *dest_op;
	x86_op_t *src_op;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 2);

	dest_op = x86_operand_1st(insn);
	src_op  = x86_operand_2nd(insn);

	dest_size = x86_operand_size(dest_op);
	src_size  = x86_operand_size(src_op);
	assert(dest_size == src_size);

	assert(src_op->type == op_register);
	assert(dest_op->type == op_register);

	if (is_write) {
		int src_gp_num;
		int dest_cr_num;

		src_gp_num = kvm_reg_from_x86_reg(src_op->data.reg.id);
		if (src_gp_num == -1)
			EXIT_ERR_PATH();

		data = kvm_regs_get(regs, src_gp_num);
		data = mask_reg(data, dest_size);

		if (dest_op->data.reg.type != reg_sys)
			EXIT_ERR_PATH();
		dest_cr_num = cr_num_from_x86_reg(dest_op->data.reg.id);
		if (dest_cr_num != cr_num)
			EXIT_ERR_PATH();

		switch (dest_cr_num) {
		case 0: sregs->cr0 = data; break;
		case 2: sregs->cr2 = data; break;
		case 3: sregs->cr3 = data; break;
		case 4: sregs->cr4 = data; break;
		default: EXIT_ERR_PATH();
		}

		printf("wrote 0x%" PRIx64 " to CR%d\n", data, dest_cr_num);
	}
	else {
		int src_cr_num;
		int dest_gp_num;

		if (src_op->data.reg.type != reg_sys)
			EXIT_ERR_PATH();
		src_cr_num = cr_num_from_x86_reg(src_op->data.reg.id);
		if (src_cr_num != cr_num)
			EXIT_ERR_PATH();

		switch (src_cr_num) {
		case 0: data = sregs->cr0; break;
		case 2: data = sregs->cr2; break;
		case 3: data = sregs->cr3; break;
		case 4: data = sregs->cr4; break;
		default: EXIT_ERR_PATH();
		}
		data = mask_reg(data, dest_size);

		dest_gp_num = kvm_reg_from_x86_reg(dest_op->data.reg.id);
		if (dest_gp_num == -1)
			EXIT_ERR_PATH();

		kvm_regs_set(regs, dest_gp_num, data);
	}

	return 0;
}

static int
emulate_cr_lmsw(kvm_context_t kvm,
		struct kvm_regs *regs,
		struct kvm_sregs *sregs,
		x86_insn_t *insn)
{
	x86_op_t *src_op;
	int src_gp_num;
	uint64_t data;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 1);

	src_op  = x86_operand_1st(insn);
	assert(x86_operand_size(src_op) == 2);

	if (src_op->type != op_register)
		EXIT();

	src_gp_num = kvm_reg_from_x86_reg(src_op->data.reg.id);
	if (src_gp_num == -1)
		EXIT_ERR_PATH();

	data = kvm_regs_get(regs, src_gp_num);

	/* from the manual: LMSW can set CR0.PE, but cannot clear it */
	if ((data & 0x1) == 0 && (sregs->cr0 & 0x1) == 1)
		EXIT_ERR_PATH();

	sregs->cr0 = (sregs->cr0 & 0xFFFFFFFFFFFFFFF0) |
		     (data       & 0x000000000000000F);

	return 0;
}

static int
emulate_cr_smsw(kvm_context_t kvm,
		struct kvm_regs *regs,
		struct kvm_sregs *sregs,
		x86_insn_t *insn)
{
	x86_op_t *dest_op;
	int dest_gp_num;
	uint64_t dest_data;
	uint64_t data;

	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 1);

	dest_op  = x86_operand_1st(insn);

	if (dest_op->type != op_register)
		EXIT();

	dest_gp_num = kvm_reg_from_x86_reg(dest_op->data.reg.id);
	if (dest_gp_num == -1)
		EXIT_ERR_PATH();

	dest_data = kvm_regs_get(regs, dest_gp_num);
	switch (x86_operand_size(dest_op)) {
		case 2:
			data = (sregs->cr0 & 0x000000000000FFFF) |
			       (dest_data  & 0xFFFFFFFFFFFF0000);
			break;
		case 4:
			data = (sregs->cr0 & 0x00000000FFFFFFFF) |
			       (dest_data  & 0xFFFFFFFF00000000);
			break;
		case 8:
			data = (sregs->cr0 & 0xFFFFFFFFFFFFFFFF) |
			       (dest_data  & 0x0000000000000000);
			break;
		default:
			EXIT_ERR_PATH();
	}

	kvm_regs_set(regs, dest_gp_num, data);
	return 0;
}


static int
emulate_cr_clts(kvm_context_t kvm,
		struct kvm_regs *regs,
		struct kvm_sregs *sregs,
		x86_insn_t *insn)
{
	assert(get_repeat_prefix(insn) == insn_no_prefix);
	assert(insn->explicit_count == 0);

	sregs->cr0 &= ~0x4L;
	return 0;
}

int
emulate_cr(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     int cr_num,
	     bool is_write)
{
	x86_insn_t insn;
	uint64_t loc, next_rip;
	unsigned int insn_size;
	int error = 0;

	libdisasm_init(reg_size());

	loc = sregs->cs.base + regs->rip;
	insn_size = get_x86_insn(loc, &insn);
	if (insn_size == -1)
		EXIT_ERR_PATH();

	printf("rip: %" PRIx64 "\n", regs->rip);
	printf("loc: %" PRIx64 "\n", loc);
	next_rip = regs->rip + insn_size;


	switch (insn.type) {

	case insn_mov:
		error = emulate_cr_mov(kvm, regs, sregs, cr_num, is_write, &insn);
		break;
	case insn_lmsw:
		if (!is_write)
			EXIT_ERR_PATH();
		error = emulate_cr_lmsw(kvm, regs, sregs, &insn);
		break;
	case insn_smsw:
		if (is_write)
			EXIT_ERR_PATH();
		error = emulate_cr_smsw(kvm, regs, sregs, &insn);
		break;
	case insn_clts:
		if (!is_write)
			EXIT_ERR_PATH();
		error = emulate_cr_clts(kvm, regs, sregs, &insn);
		break;
	default:
		printf("insn.type = 0x%x\n", insn.type);
		EXIT();
	}

	if (error == 0)
		regs->rip = next_rip;

	libdisasm_cleanup();

	return error;
}
