/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#define FKVM_INTERNAL
#include <sys/fkvm.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <sys/syscall.h>
#include <sys/param.h>

#include "libkvm.h"
#include "libfkvm-common.h"
#include "disasm.h"

static inline bool
_is_valid_vcpu_slot(int slot, const char *func)
{
	bool valid = (slot >= 0 && slot < MAX_VCPUS);
	if (!valid)
		fprintf(stderr, "%s: Invalid vcpu slot provided: %d\n",
		                func, slot);
	return valid;
}
#define is_valid_vcpu_slot(slot) _is_valid_vcpu_slot((slot), __FUNCTION__)

void
cpu_virtual_memory_rw(unsigned long gvaddr,
		      uint8_t *buf,
		      int len,
		      int is_write)
{
	unsigned long gpaddr;

	while (len > 0) {

		int on_this_page;

		on_this_page = MIN(len, PAGE_SIZE - (gvaddr & PAGE_MASK)); // 1 - 4096

		gpaddr = kvm_get_phys_addr(gvaddr);
		if (gpaddr == -1)
			EXIT_ERR_PATH();

#if 0
		printf("guest virtual 0x%lx -> physical 0x%lx\n", gvaddr, gpaddr);
#endif

		cpu_physical_memory_rw(gpaddr, buf, on_this_page, is_write);

		len -= on_this_page;
	}
}

/* Helper Functions */
static int
handle_exit_io(kvm_context_t kvm, int vcpu, struct kvm_run *kvm_run)
{
	struct kvm_regs regs;
	struct kvm_sregs sregs;
	int error;

	error = kvm_get_regs(kvm, vcpu, &regs);
	if (error != 0)
		return error;

	error = kvm_get_sregs(kvm, vcpu, &sregs);
	if (error != 0)
		return error;

	error = emulate_ioio(kvm,
			     &regs,
			     &sregs,
			     kvm_run->u.io.port,
			     kvm_run->u.io.size,
			     kvm_run->u.io.rep,
			     kvm_run->u.io.string,
			     kvm_run->u.io.in);
	if (error != 0)
		return error;

	regs.rip = kvm_run->u.io.next_rip;

	error = kvm_set_regs(kvm, vcpu, &regs);
	if (error != 0)
		return error;

	return 0;
}

static int
handle_exit_mmio(kvm_context_t kvm, int vcpu, struct kvm_run *kvm_run)
{
	/* TODO: These regs structures and the syscalls needed to fill them 
	 * should be eliminated when kvm_run is implemented with a shared page */
	struct kvm_regs regs;
	struct kvm_sregs sregs;
	int error;
	int error2;

#if 0
	printf("Memory Mapped IO Call\n");
	printf("Guest Physical Address : 0x%" PRIx64 "\n",
	       kvm_run->u.mmio.fault_gpa);
#endif

	error = kvm_get_regs(kvm, vcpu, &regs);
	if (error != 0)
		return error;

	error = kvm_get_sregs(kvm, vcpu, &sregs);
	if (error != 0)
		return error;

	error = emulate_mmio(kvm, &regs, &sregs, kvm_run->u.mmio.fault_gpa);
	if (error != 0)
		EXIT_ERR_PATH();

#if 0
	loc = kvm_run->u.mmio.rip + kvm_run->u.mmio.cs_base;
	printf("Instruction Pointer    : 0x%" PRIx64 "\n", kvm_run->u.mmio.rip);
	printf("CS Base Address        : 0x%" PRIx64 "\n", kvm_run->u.mmio.cs_base);
	printf("ES Base Address        : 0x%" PRIx64 "\n", sregs.es.base);
	printf("rIP+cs_base            : 0x%" PRIx64 "\n", loc);
	printf("Guest Base             : %p\n", get_guest_base(kvm));
	printf("RSI                    : 0x%" PRIx64 "\n", regs.rsi);
	printf("RSI                    : 0x%" PRIx64 "\n", kvm_regs_get(&regs, KVM_REG_RSI));
	printf("RDI                    : 0x%" PRIx64 "\n", regs.rdi);
	printf("RDI                    : 0x%" PRIx64 "\n", kvm_regs_get(&regs, KVM_REG_RDI));
	printf("RCX                    : 0x%" PRIx64 "\n", kvm_regs_get(&regs, KVM_REG_RCX));
	printf("default operand size   : %d\n", (int)disasm_inst.operand_size);
#endif

	error2 = kvm_set_regs(kvm, vcpu, &regs);
	if (error2 != 0)
		EXIT_ERR_PATH();

	return error;
}

static int
handle_exit_cr(kvm_context_t kvm, int vcpu, struct kvm_run *kvm_run)
{
	int error;
	int cr_num;
	bool is_write;
	struct kvm_regs regs;
	struct kvm_sregs sregs;

	error = 0;
	cr_num = kvm_run->u.cr.cr_num;
	is_write = kvm_run->u.cr.is_write;

	if (is_write) {
		switch (cr_num) {
		case 0:
		case 3:
		case 4:
			break;
		default:
			EXIT_ERR_PATH();
			break;
		}
	}
	else {
		switch (cr_num) {
		case 0:
		case 3:
		case 4:
			break;
		default:
			EXIT_ERR_PATH();
			break;
		}
	}

	error = kvm_get_regs(kvm, vcpu, &regs);
	if (error != 0)
		return error;

	error = kvm_get_sregs(kvm, vcpu, &sregs);
	if (error != 0)
		return error;

	error = emulate_cr(kvm, &regs, &sregs, cr_num, is_write);
	if (error != 0)
		return error;

	error = kvm_set_regs(kvm, vcpu, &regs);
	if (error != 0)
		EXIT_ERR_PATH();

	error = kvm_set_sregs(kvm, vcpu, &sregs);
	if (error != 0)
		EXIT_ERR_PATH();

	return 0;
}

static struct kvm_cpuid_entry *
get_cpuid_entry(struct kvm_context *kvm, int vcpu, int func)
{
	int i;
	for (i = 0; i < kvm->ncpuid_entries[vcpu]; i++) {
		if (kvm->cpuid_entries[vcpu][i].function == func)
			return &kvm->cpuid_entries[vcpu][i];
	}
	return NULL;
}


/* LIBKVM Functions */

/* TODO: stop faking this function */
struct kvm_msr_list *
kvm_get_msr_list(kvm_context_t kvm)
{
	struct kvm_msr_list *msrs;

	msrs = malloc(sizeof(*msrs) + 1*sizeof(*msrs->indices));
	if (msrs == NULL)
		return NULL;

	msrs->nmsrs = 1;
	msrs->indices[0] = MSR_STAR;
	return msrs;
}

int
kvm_get_msrs(kvm_context_t kvm, int vcpu, struct kvm_msr_entry *msrs, int n)
{
	int error;

	error = syscall(SYS_fkvm_set_regs, FKVM_REGS_TYPE_MSRS, msrs, n);
	if (error != 0) {
		printf("kvm_set_msrs failed (errno=%d)\n", errno);
		return -1;
	}

	return 0;
}

int
kvm_set_msrs(kvm_context_t kvm, int vcpu, struct kvm_msr_entry *msrs, int n)
{
	int error;

	{
		int i;
		printf("kvm_set_msrs:\n");
		for (i = 0; i < n; i++) {
			printf("idx: 0x%" PRIx32 " to data: 0x%" PRIx64 "\n",
			       msrs[i].index, msrs[i].data);
		}
		printf("\n");
	}

	error = syscall(SYS_fkvm_set_regs, FKVM_REGS_TYPE_MSRS, msrs, n);
	if (error != 0) {
		printf("kvm_set_msrs failed (errno=%d)\n", errno);
		return -1;
	}

	return 0;
}

int
kvm_dirty_pages_log_enable_slot(kvm_context_t kvm,
				uint64_t phys_start, uint64_t len)
{
	fprintf(stderr, "WARNING: we just ignored kvm_dirty_pages_log_enable_slot\n");
	fprintf(stderr, "dirty_pages_log_enable_slot: %" PRIx64 " len %" PRIx64 "\n", phys_start, len);
	//EXIT();
	return -ENOSYS;
}

int
kvm_dirty_pages_log_disable_slot(kvm_context_t kvm,
				 uint64_t phys_start, uint64_t len)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_inject_nmi(kvm_context_t kvm, int vcpu)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_is_ready_for_nmi_injection(kvm_context_t kvm, int vcpu)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_enable_vapic(kvm_context_t kvm, int vcpu, uint64_t vapic)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_enable_tpr_access_reporting(kvm_context_t kvm, int vcpu)
{
	fprintf(stderr, "WARNING: we just ignored kvm_enable_tpr_access\n");
	/* TODO: we shouldn't just ignore this request... */
	return -1;
}

kvm_context_t
kvm_init(struct kvm_callbacks *callbacks, void *opaque)
{
	kvm_context_t kvm;

	kvm = malloc(sizeof(*kvm));
	if (kvm == NULL)
		return NULL;
	memset(kvm, 0, sizeof(*kvm));
	kvm->callbacks = callbacks;
	kvm->opaque = opaque;

	return kvm;
}

void
kvm_finalize(kvm_context_t kvm)
{
	int i;

	for (i = 0; i < MAX_VCPUS; i++) {
		if(kvm->kvm_run[i] != NULL)
			free(kvm->kvm_run[i]);
	}

	free(kvm);

	return;
}

void
kvm_disable_irqchip_creation(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return;
}

void
kvm_disable_pit_creation(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return;
}

int
kvm_create(kvm_context_t kvm, unsigned long phys_mem_bytes, void **phys_mem)
{
	int r;

	r = kvm_create_vm(kvm);
	//kvm_arch_create(kvm, ...);

	kvm->phys_mem_bytes = phys_mem_bytes;
	kvm->phys_mem_ptr = phys_mem;
	return r;
}

int
kvm_create_vm(kvm_context_t kvm)
{
	int r;

	r = syscall(SYS_fkvm_create_vm);
	if (r != 0)
		return -1;

	return 0;
}

int
kvm_check_extension(kvm_context_t kvm, int ext)
{
	EXIT_API_STUB();
	return -1;
}

void
kvm_create_irqchip(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return;
}

int
kvm_create_vcpu(kvm_context_t kvm, int slot)
{
	int error;
	struct kvm_run *run;

	if (!is_valid_vcpu_slot(slot))
		return -1;

	run = malloc(sizeof(*run));
	if (run == NULL)
		return -1;

	error = syscall(SYS_fkvm_create_vcpu);
	if (error != 0) {
		printf("kvm_create_vcpu failed\n");
		free(run);
		return -1;
	}

	memset(run, 0, sizeof(*run));
	kvm->kvm_run[slot] = run;

	return 0;
}

int
kvm_run(kvm_context_t kvm, int vcpu)
{
	struct kvm_run *kvm_run;
	int error = 0;

	//printf("kvm_run: entering\n");

	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	kvm_run = kvm->kvm_run[vcpu];

	while (error == 0)
	{
#if 0
		struct kvm_regs regs;
		struct kvm_sregs sregs;
#endif

		kvm_run->request_interrupt_window = kvm->callbacks->try_push_interrupts(kvm->opaque);

		error = kvm->callbacks->pre_kvm_run(kvm->opaque, vcpu);
		if (error != 0)
			return error;

#if 0
		kvm_get_regs(kvm, vcpu, &regs);
		kvm_get_sregs(kvm, vcpu, &sregs);
		printf("Begin kvm_run\n");
		printf("Rip = %" PRIx64 "\n", regs.rip);
		printf("cs.base = %" PRIx64 "\n", sregs.cs.base);
		printf("\n");
#endif

		error = syscall(SYS_fkvm_vm_run, kvm_run);
		if (error != 0) {
			printf("kvm_run failed (errno %d)\n", errno);
			printf("exit_reason: %d\n", kvm_run->exit_reason);
			kvm->callbacks->post_kvm_run(kvm->opaque, vcpu);
			return -1;
		}

#if 0
		kvm_get_regs(kvm, vcpu, &regs);
		kvm_get_sregs(kvm, vcpu, &sregs);
		printf("After kvm_run\n");
		printf("Rip = %" PRIx64 "\n", regs.rip);
		printf("cs.base = %" PRIx64 "\n", sregs.cs.base);
		printf("exit_reason: %d\n", kvm_run->exit_reason);
		printf("\n");
#endif

		kvm->callbacks->post_kvm_run(kvm->opaque, vcpu);

		switch(kvm_run->exit_reason) {

		case KVM_EXIT_UNKNOWN: {
			EXIT();
			break;
		}

		case KVM_EXIT_EXCEPTION: {
			EXIT();
			break;
		}

		case KVM_EXIT_IO: {
			error = handle_exit_io(kvm, vcpu, kvm_run);
			break;
		}

		case KVM_EXIT_HYPERCALL: {
			EXIT();
			break;
		}

		case KVM_EXIT_DEBUG: {
			EXIT();
			break;
		}

		case KVM_EXIT_HLT: {
			struct kvm_regs regs;
			struct kvm_sregs sregs;
			kvm_get_regs(kvm, vcpu, &regs);
			kvm_get_sregs(kvm, vcpu, &sregs);
			printf("KVM_EXIT_HLT:\n");
			printf("rip     = %" PRIx64 "\n", regs.rip);
			printf("cs.base = %" PRIx64 "\n", sregs.cs.base);

			error = kvm->callbacks->halt(kvm->opaque, vcpu);
			/* error should be 1, we want to exit this loop */
			if (error != 1)
				EXIT_ERR_PATH();
			break;
		}

		case KVM_EXIT_MMIO: {
			error = handle_exit_mmio(kvm, vcpu, kvm_run);
			break;
		}

		case KVM_EXIT_IRQ_WINDOW_OPEN: {
			EXIT();
			break;
		}

		case KVM_EXIT_SHUTDOWN: {
			EXIT();
			break;
		}

		case KVM_EXIT_FAIL_ENTRY: {
			EXIT();
			break;
		}

		case KVM_EXIT_INTR: {
			EXIT();
			break;
		}

		case KVM_EXIT_S390_SIEIC: {
			EXIT();
			break;
		}

		case KVM_EXIT_S390_RESET: {
			EXIT();
			break;
		}

		case KVM_EXIT_DCR: {
			EXIT();
			break;
		}

		case KVM_EXIT_NMI: {
			EXIT();
			break;
		}

		case KVM_EXIT_NMI_WINDOW_OPEN: {
			EXIT();
			break;
		}

		case KVM_EXIT_CPUID: {
			struct kvm_regs regs;
			uint32_t func;
			struct kvm_cpuid_entry *cpuid_entry;

			func = kvm_run->u.cpuid.fn;

			error = kvm_get_regs(kvm, vcpu, &regs);
			if (error != 0)
				EXIT_ERR_PATH();

			cpuid_entry = get_cpuid_entry(kvm, vcpu, func);
			if (cpuid_entry == NULL) {
				printf("CPUID func 0x%" PRIx32" not found\n", func);

				cpuid_entry = get_cpuid_entry(kvm, vcpu, 0);
				if (cpuid_entry == NULL)
					EXIT_ERR_PATH();

				func = cpuid_entry->eax;
				printf("Using highest basic info leaf 0x%" PRIx32" not found\n", func);

				cpuid_entry = get_cpuid_entry(kvm, vcpu, func);
				if (cpuid_entry == NULL)
					EXIT_ERR_PATH();
			}

			regs.rax = cpuid_entry->eax;
			regs.rbx = cpuid_entry->ebx;
			regs.rcx = cpuid_entry->ecx;
			regs.rdx = cpuid_entry->edx;
			regs.rip += 2;

			printf("\n");
			printf("CPUID func 0x%" PRIx32 ":\n", func);
			printf("eax: 0x%" PRIx32 "\n", (uint32_t) regs.rax);
			printf("ebx: 0x%" PRIx32 "\n", (uint32_t) regs.rbx);
			printf("ecx: 0x%" PRIx32 "\n", (uint32_t) regs.rcx);
			printf("edx: 0x%" PRIx32 "\n", (uint32_t) regs.rdx);
			printf("\n");

			error = kvm_set_regs(kvm, vcpu, &regs);
			if (error != 0)
				EXIT_ERR_PATH();

			break;
		}

		case KVM_EXIT_CR: {
			error = handle_exit_cr(kvm, vcpu, kvm_run);
			if (error != 0)
				EXIT_ERR_PATH();
			break;
		}

		case KVM_EXIT_DR: {
			EXIT();
			break;
		}

		case KVM_EXIT_EXCP: {
			struct kvm_regs regs;
			struct kvm_sregs sregs;
			x86_insn_t insn;
			kvm_get_regs(kvm, vcpu, &regs);
			kvm_get_sregs(kvm, vcpu, &sregs);
			printf("KVM_EXIT_EXCP\n");
			(void) get_x86_insn(sregs.cs.base + regs.rip, &insn);
			EXIT();
			break;
		}

		case KVM_EXIT_CONTINUE: {
			return 0;
		}

		default: {
			EXIT();
			break;
		}

		}
	}

	return error;
}

int
kvm_get_interrupt_flag(kvm_context_t kvm, int vcpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	return kvm->kvm_run[vcpu]->if_flag;
}

uint64_t
kvm_get_apic_base(kvm_context_t kvm, int vcpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	return 0xfee00000;
}

int
kvm_is_ready_for_interrupt_injection(kvm_context_t kvm, int vcpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	return kvm->kvm_run[vcpu]->ready_for_interrupt_injection;
}

int
kvm_get_regs(kvm_context_t kvm, int vcpu, struct kvm_regs *regs)
{
	int error;

	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	error = syscall(SYS_fkvm_get_regs, FKVM_REGS_TYPE_REGS, regs, 0);
	if (error != 0) {
		printf("kvm_set_regs failed (errno=%d)\n", errno);
		return -1;
	}

	return 0;
}

int
kvm_set_regs(kvm_context_t kvm, int vcpu, struct kvm_regs *regs)
{
	int error;

	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	error = syscall(SYS_fkvm_set_regs, FKVM_REGS_TYPE_REGS, regs, 0);
	if (error != 0) {
		printf("kvm_set_regs failed (errno=%d)\n", errno);
		return -1;
	}

	return 0;
}

struct kvm_fpu fpus[MAX_VCPUS]; // TODO: temporary.  For faking

int
kvm_get_fpu(kvm_context_t kvm, int vcpu, struct kvm_fpu *fpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	*fpu = fpus[vcpu];

	//fprintf(stderr, "WARNING: we just ignored kvm_get_fpu\n");
	/* TODO: we shouldn't just ignore this request... */
	return -ENOSYS;
}

int
kvm_set_fpu(kvm_context_t kvm, int vcpu, struct kvm_fpu *fpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	fpus[vcpu] = *fpu;

	fprintf(stderr, "WARNING: we just ignored kvm_set_fpu\n");
	/* TODO: we shouldn't just ignore this request... */
	return -ENOSYS;
}

int
kvm_get_sregs(kvm_context_t kvm, int vcpu, struct kvm_sregs *regs)
{
	int error;

	if(!is_valid_vcpu_slot(vcpu))
		return -1;

	error = syscall(SYS_fkvm_get_regs, FKVM_REGS_TYPE_SREGS, regs, 0);
	if (error != 0) {
		printf("kvm_get_sregs failed (errno=%d)\n", errno);
		return -1;
	}
	return 0;
}

int
kvm_set_sregs(kvm_context_t kvm, int vcpu, struct kvm_sregs *regs)
{
	int error;

	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	error = syscall(SYS_fkvm_set_regs, FKVM_REGS_TYPE_SREGS, regs, 0);
	if (error != 0) {
		printf("kvm_set_sregs failed (errno=%d)\n", errno);
		return -1;
	}

	return 0;
}

int
kvm_get_mpstate(kvm_context_t kvm, int vcpu, void *mp_state)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_set_mpstate(kvm_context_t kvm, int vcpu, void *mp_state)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_inject_irq(kvm_context_t kvm, int vcpu, unsigned irq)
{
	int error;
	error = syscall(SYS_fkvm_inject_virq, irq);
	if (error != 0) {
		printf("kvm_inject_irq failed (errno=%d)\n", errno);
		return -1;
	}
	return 0;
}

int
kvm_guest_debug(kvm_context_t kvm, int vcpu, struct kvm_debug_guest *dbg)
{
	EXIT_API_STUB();
	return -1;
}

/* TODO: it'd be easy to move this into the kernel for fewer vm_run round-trips.
         add some stats to see if its worth it */
int
kvm_setup_cpuid(kvm_context_t kvm, int vcpu,
		int nent, const struct kvm_cpuid_entry *entries)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	if (kvm->cpuid_entries[vcpu] != NULL)
		free(kvm->cpuid_entries[vcpu]);
	kvm->ncpuid_entries[vcpu] = 0;

	kvm->cpuid_entries[vcpu] = malloc(sizeof(entries[0]) * nent);
	if (kvm->cpuid_entries[vcpu] == NULL)
		return -1;
	/* TODO: free this when finalizing vcpu */

	memcpy(kvm->cpuid_entries[vcpu], entries, sizeof(entries[0]) * nent);
	kvm->ncpuid_entries[vcpu] = nent;
	return 0;
}

int
kvm_set_shadow_pages(kvm_context_t kvm, unsigned int nrshadow_pages)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_shadow_pages(kvm_context_t kvm , unsigned int *nrshadow_pages)
{
	EXIT_API_STUB();
	return -1;
}

void
kvm_set_cr8(kvm_context_t kvm, int vcpu, uint64_t cr8)
{
	if (!is_valid_vcpu_slot(vcpu))
		return;

	kvm->kvm_run[vcpu]->cr8 = cr8;
}

uint64_t
kvm_get_cr8(kvm_context_t kvm, int vcpu)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	return kvm->kvm_run[vcpu]->cr8;
}

int
kvm_set_signal_mask(kvm_context_t kvm, int vcpu, const sigset_t *sigset)
{
	if (!is_valid_vcpu_slot(vcpu))
		return -1;

	fprintf(stderr, "WARNING: we just ignored kvm_set_signal_mask\n");
	/* TODO: we shouldn't just ignore this request... */
	return -ENOSYS;
}

int
kvm_dump_vcpu(kvm_context_t kvm, int vcpu)
{
	EXIT_API_STUB();
	return -1;
}

void
kvm_show_regs(kvm_context_t kvm, int vcpu)
{
	EXIT_API_STUB();
	return;
}


void *
kvm_create_phys_mem(kvm_context_t kvm , unsigned long phys_start, 
			  unsigned long len, int log, int writable)
{
	EXIT_API_STUB();
	return NULL;
}

void
kvm_destroy_phys_mem(kvm_context_t kvm, unsigned long phys_start,
			  unsigned long len)
{
	int error;

	error = syscall(SYS_fkvm_unset_user_mem_region, len, phys_start);
	if (error == -1) {
		fprintf(stderr, "destroy_userspace_phys_mem: %s",
			strerror(errno));
	}

	return;
}

int
kvm_is_intersecting_mem(kvm_context_t kvm, unsigned long phys_start)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_is_containing_region(kvm_context_t kvm,
			 unsigned long phys_start, unsigned long size)
{
	int error;

	/* The fkvm_set_user_mem_region checks for a containing region
	 * when userspace_addr = NULL */
	error = syscall(SYS_fkvm_set_user_mem_region, NULL, size, phys_start);
	if (error == 0)
		return 1;
	if (errno == EFAULT) {
		return 0;
	}
	else {
		fprintf(stderr, "kvm_is_containing_region: %s\n",
		        strerror(errno));
		return -1;
	}
}

int
kvm_register_phys_mem(kvm_context_t kvm, unsigned long phys_start,
		      void *userspace_addr, unsigned long len, int log)
{
	int error;

	error = syscall(SYS_fkvm_set_user_mem_region,
			userspace_addr, len, phys_start);

	if (error != 0) {
		fprintf(stderr, "create_userspace_phys_mem: %s\n",
		        strerror(errno));
		return -1;
	}

	return 0;
}

void
kvm_unregister_memory_area(kvm_context_t kvm, uint64_t phys_start,
			   unsigned long len)
{
	kvm_destroy_phys_mem(kvm, phys_start, len);
	return;
}

int
kvm_is_allocated_mem(kvm_context_t kvm, unsigned long phys_start,
		     unsigned long len)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_create_mem_hole(kvm_context_t kvm, unsigned long phys_start,
		    unsigned long len)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_register_userspace_phys_mem(kvm_context_t kvm,
			unsigned long phys_start, void *userspace_addr,
			unsigned long len, int log)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_dirty_pages(kvm_context_t kvm, unsigned long phys_addr, void *buf)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_dirty_pages_range(kvm_context_t kvm, unsigned long phys_addr,
			  unsigned long end_addr, void *buf, void*opaque,
			  int (*cb)(unsigned long start, unsigned long len,
				    void*bitmap, void *opaque))
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_register_coalesced_mmio(kvm_context_t kvm,
			    uint64_t addr, uint32_t size)
{
	/* let's act like we can't do this... */
	return -ENOSYS;
}

int
kvm_unregister_coalesced_mmio(kvm_context_t kvm,
			      uint64_t addr, uint32_t size)
{
	/* let's act like we can't do this... */
	return -ENOSYS;
}

int
kvm_create_memory_alias(kvm_context_t kvm,
			uint64_t phys_start, uint64_t len,
			uint64_t target_phys)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_destroy_memory_alias(kvm_context_t kvm, uint64_t phys_start)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_mem_map(kvm_context_t kvm, unsigned long phys_addr, void *bitmap)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_mem_map_range(kvm_context_t kvm, unsigned long phys_addr,
		      unsigned long len, void *buf, void *opaque,
		      int (*cb)(unsigned long start,unsigned long len,
			        void* bitmap, void* opaque))
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_set_irq_level(kvm_context_t kvm, int irq, int level)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_dirty_pages_log_enable_all(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_dirty_pages_log_reset(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_irqchip_in_kernel(kvm_context_t kvm)
{
	return 0;
}

int
kvm_has_sync_mmu(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_irqchip(kvm_context_t kvm, void *chip)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_set_irqchip(kvm_context_t kvm, void *chip)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_get_lapic(kvm_context_t kvm, int vcpu, void *s)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_set_lapic(kvm_context_t kvm, int vcpu, void *s)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_pit_in_kernel(kvm_context_t kvm)
{
	EXIT_API_STUB();
	return -1;
}

int
kvm_init_coalesced_mmio(kvm_context_t kvm)
{
	/* let's act like we can't do this... */
	return -ENOSYS;
}
