#ifndef DISASM_H
#define DISASM_H
#include <libdis.h>

static inline int
get_repeat_prefix(x86_insn_t *insn)
{
	return (insn->prefix & 0x00F);
}

int
kvm_reg_from_x86_reg(int x86_reg_id);

int
cr_num_from_x86_reg(int x86_reg_id);

struct kvm_segment*
kvm_seg_from_x86_op(struct kvm_sregs *sregs, x86_op_t *op);

uint64_t
get_source_data(struct kvm_regs *regs, x86_op_t *op);

uint64_t
get_memi_address(struct kvm_regs *regs, struct kvm_sregs *sregs,
                 x86_op_t *op, size_t size);

unsigned int
get_x86_insn(const uint64_t insn_addr, x86_insn_t *insn);

void
libdisasm_init(size_t reg_size);

void
libdisasm_cleanup();

#endif
