/*-
 * Copyright (c) 2008 Brent Stephens <brents@rice.edu>
 * Copyright (c) 2008 Diego Ongaro <diego.ongaro@rice.edu>
 * Copyright (c) 2008 Oleg Pesok <olegpesok@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef FKVM_COMMON_H
#define FKVM_COMMON_H

#include <sys/fkvm.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <stdio.h>
#include <inttypes.h>

#include "libkvm.h"

/* HARDWARE DEFINITIONS */

#define MSR_EFER_LMA	(1 << 10)
#define CR0_PE_MASK	(1 << 0)
#define RFLAGS_VM	(1 << 17)

#define RFLAGS_DF_SHIFT 10
#define RFLAGS_DF_MASK (1 << RFLAGS_DF_SHIFT)

#define CF_MASK 0x0001
#define PF_MASK 0x0002
#define AF_MASK 0x0008
#define ZF_MASK 0x0020
#define SF_MASK 0x0040
#define OF_MASK 0x0400

#define MSR_STAR 0xc0000081

/* MISC MACROS */

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

static inline void
_fkvm_exit(const char *func, int line, const char *msg)
{
	fprintf(stderr, "libfkvm: Error in function %s on line %d: %s\n",
			func, line, msg);
	sleep(10);
	exit(1);
}

#define EXIT_API_STUB() _fkvm_exit(__FUNCTION__, __LINE__, \
				   "stubbed API function not implemented")
#define EXIT_ERR_PATH() _fkvm_exit(__FUNCTION__, __LINE__, \
				   "error path not implemented")
#define EXIT()          _fkvm_exit(__FUNCTION__, __LINE__, \
				   "(no message provided)")


/* QEMU PROTOTYPES  */
/* These should really callbacks... */

extern void
cpu_physical_memory_rw(unsigned long addr, uint8_t *buf, int len, int is_write);

extern unsigned long kvm_get_phys_addr(unsigned long gvaddr);

/* LIBFKVM INTERNAL DATA STRUCTURES */

struct kvm_context {
	// Callbacks that KVM uses to emulate various unvirtualizable functionality
	struct kvm_callbacks *callbacks;
	void *opaque;
	struct kvm_run *kvm_run[MAX_VCPUS];
	unsigned long phys_mem_bytes;
	void **phys_mem_ptr;
	struct kvm_cpuid_entry *cpuid_entries[MAX_VCPUS];
	int ncpuid_entries[MAX_VCPUS];
};

static inline void *
get_guest_base(struct kvm_context *kvm) {
	return *kvm->phys_mem_ptr;
}

void
cpu_virtual_memory_rw(unsigned long gvaddr, uint8_t *buf, int len, int is_write);

/* SHARED EMULATION HELPERS */

int
emulate_ioio(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint16_t port,
	     size_t size,
	     bool rep,
	     bool string,
	     bool in);

int
emulate_mmio(kvm_context_t kvm,
	     struct kvm_regs *regs,
	     struct kvm_sregs *sregs,
	     uint64_t fault_addr);

int
emulate_cr(kvm_context_t kvm,
	   struct kvm_regs *regs,
	   struct kvm_sregs *sregs,
	   int cr_num,
	   bool is_write);

static uint64_t
mask_reg(uint64_t reg_value, size_t size)
{
	switch (size) {
		case 1:
			reg_value &= 0x00000000000000FF;
			break;
		case 2:
			reg_value &= 0x000000000000FFFF;
			break;
		case 4:
			reg_value &= 0x00000000FFFFFFFF;
			break;
		case 8:
			reg_value &= 0xFFFFFFFFFFFFFFFF;
			break;
		default:
			assert(0);
			return -1;
	}

	return reg_value;
}

static inline bool
_test_repeat_noop(const bool repeat, const size_t reg_size, struct kvm_regs *regs)
{
	if (repeat) {
		return mask_reg(regs->rcx, reg_size) == 0;
	}
	else {
		return false;
	}
}

static inline bool
_test_repeat_tail(const bool repeat, const size_t reg_size, struct kvm_regs *regs)
{
	if (repeat) {
		regs->rcx--;
		return mask_reg(regs->rcx, reg_size) != 0;
	}
	else {
		return false;
	}
}

/* XXX: What if 16 mode is trying to do 32-bit accesses? or other oddities? 
 * This is clearly broken and should be addressed */
static inline size_t
_reg_size(const struct kvm_regs *regs, const struct kvm_sregs *sregs)
{
	if ((sregs->efer & MSR_EFER_LMA) != 0) {
		return 8; //Long Mode
	}
	else if ((sregs->cr0 & CR0_PE_MASK) != 0) {
		if ((regs->rflags & RFLAGS_VM) != 0) {
			return 2; //Virtual-8086 Mode
		}
		else {
			//TODO: verify that the default size for Protected Mode is 4 bytes
			return 4; //Protected Mode
		}
	}
	else {
		return 2; //Real Mode
	}
}

#endif
