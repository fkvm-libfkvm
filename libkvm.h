#ifndef LIBKVM_H
#define LIBKVM_H

#define __u8  uint8_t
#define __u16 uint16_t
#define __u32 uint32_t
#define __u64 uint64_t


#include <stdint.h>

#ifndef __user
#define __user 
#endif

#include <sys/fkvm.h>

#include <signal.h>

struct kvm_context;

typedef struct kvm_context *kvm_context_t;

struct kvm_msr_list *kvm_get_msr_list(kvm_context_t);

struct kvm_callbacks {
    int (*inb)(void *opaque, uint16_t addr, uint8_t *data);
    int (*inw)(void *opaque, uint16_t addr, uint16_t *data);
    int (*inl)(void *opaque, uint16_t addr, uint32_t *data);
    int (*outb)(void *opaque, uint16_t addr, uint8_t data);
    int (*outw)(void *opaque, uint16_t addr, uint16_t data);
    int (*outl)(void *opaque, uint16_t addr, uint32_t data);
    int (*mmio_read)(void *opaque, uint64_t addr, uint8_t *data, int len);
    int (*mmio_write)(void *opaque, uint64_t addr, uint8_t *data, int len);
    int (*debug)(void *opaque, int vcpu);
    int (*halt)(void *opaque, int vcpu);
    int (*shutdown)(void *opaque, int vcpu);
    int (*io_window)(void *opaque);
    int (*try_push_interrupts)(void *opaque);
    int (*try_push_nmi)(void *opaque);
    void (*post_kvm_run)(void *opaque, int vcpu);
    int (*pre_kvm_run)(void *opaque, int vcpu);
    int (*tpr_access)(void *opaque, int vcpu, uint64_t rip, int is_write);
};

int kvm_get_msrs(kvm_context_t, int vcpu, struct kvm_msr_entry *msrs, int n);
int kvm_set_msrs(kvm_context_t, int vcpu, struct kvm_msr_entry *msrs, int n);
kvm_context_t kvm_init(struct kvm_callbacks *callbacks, void *opaque); 
void kvm_finalize(kvm_context_t kvm);
void kvm_disable_irqchip_creation(kvm_context_t);
void kvm_disable_pit_creation(kvm_context_t);
int kvm_create(kvm_context_t, unsigned long phys_mem_bytes, void **phys_mem);
int kvm_create_vm(kvm_context_t);
int kvm_check_extension(kvm_context_t, int ext);
void kvm_create_irqchip(kvm_context_t);
int kvm_create_vcpu(kvm_context_t, int slot);
int kvm_run(kvm_context_t, int vcpu);
int kvm_get_interrupt_flag(kvm_context_t, int vcpu);
uint64_t kvm_get_apic_base(kvm_context_t, int vcpu);
int kvm_is_ready_for_interrupt_injection(kvm_context_t, int vcpu);
int kvm_is_ready_for_nmi_injection(kvm_context_t, int vcpu);
int kvm_get_regs(kvm_context_t, int vcpu, struct kvm_regs *regs);
int kvm_set_regs(kvm_context_t, int vcpu, struct kvm_regs *regs);
int kvm_get_fpu(kvm_context_t, int vcpu, struct kvm_fpu *fpu);
int kvm_set_fpu(kvm_context_t, int vcpu, struct kvm_fpu *fpu);
int kvm_get_sregs(kvm_context_t, int vcpu, struct kvm_sregs *regs);
int kvm_set_sregs(kvm_context_t, int vcpu, struct kvm_sregs *regs);
int kvm_inject_irq(kvm_context_t, int vcpu, unsigned irq);
int kvm_guest_debug(kvm_context_t, int vcpu, struct kvm_debug_guest *dbg);
int kvm_setup_cpuid(kvm_context_t, int vcpu, int nent, const struct kvm_cpuid_entry *entries);
int kvm_set_shadow_pages(kvm_context_t, unsigned int nrshadow_pages);
int kvm_get_shadow_pages(kvm_context_t, unsigned int *nrshadow_pages);
void kvm_set_cr8(kvm_context_t, int vcpu, uint64_t cr8);
__u64 kvm_get_cr8(kvm_context_t, int vcpu);
int kvm_set_signal_mask(kvm_context_t, int vcpu, const sigset_t *sigset);
int kvm_dump_vcpu(kvm_context_t, int vcpu);
void kvm_show_regs(kvm_context_t, int vcpu);
void *kvm_create_phys_mem(kvm_context_t, unsigned long phys_start, unsigned long len, int log, int writable);
void kvm_destroy_phys_mem(kvm_context_t, unsigned long phys_start, unsigned long len);
void kvm_unregister_memory_area(kvm_context_t, uint64_t phys_start, unsigned long len);
int kvm_is_containing_region(kvm_context_t, unsigned long phys_start, unsigned long size);
int kvm_register_phys_mem(kvm_context_t, unsigned long phys_start, void *userspace_addr, unsigned long len, int log);
int kvm_get_dirty_pages(kvm_context_t, unsigned long phys_addr, void *buf);
int kvm_get_dirty_pages_range(kvm_context_t, unsigned long phys_addr, unsigned long end_addr, void *buf, void*opaque, int (*cb)(unsigned long start, unsigned long len, void*bitmap, void *opaque));
int kvm_register_coalesced_mmio(kvm_context_t, uint64_t addr, uint32_t size);
int kvm_unregister_coalesced_mmio(kvm_context_t, uint64_t addr, uint32_t size);
int kvm_create_memory_alias(kvm_context_t, uint64_t phys_start, uint64_t len, uint64_t target_phys);
int kvm_destroy_memory_alias(kvm_context_t, uint64_t phys_start);
int kvm_get_mem_map(kvm_context_t, unsigned long phys_addr, void *bitmap);
int kvm_get_mem_map_range(kvm_context_t, unsigned long phys_addr, unsigned long len, void *buf, void *opaque, int (*cb)(unsigned long start,unsigned long len, void* bitmap, void* opaque));
int kvm_set_irq_level(kvm_context_t, int irq, int level);
int kvm_dirty_pages_log_enable_slot(kvm_context_t, uint64_t phys_start, uint64_t len);
int kvm_dirty_pages_log_disable_slot(kvm_context_t, uint64_t phys_start, uint64_t len);
int kvm_dirty_pages_log_enable_all(kvm_context_t);
int kvm_dirty_pages_log_reset(kvm_context_t);
int kvm_irqchip_in_kernel(kvm_context_t);
int kvm_has_sync_mmu(kvm_context_t);
int kvm_pit_in_kernel(kvm_context_t);
int kvm_init_coalesced_mmio(kvm_context_t);

#ifdef KVM_CAP_MP_STATE
int kvm_get_mpstate(kvm_context_t, int vcpu, struct kvm_mp_state *mp_state);
int kvm_set_mpstate(kvm_context_t, int vcpu, struct kvm_mp_state *mp_state);
static inline int kvm_reset_mpstate(kvm_context_t, int vcpu)
{
    struct kvm_mp_state mp_state = {.mp_state = KVM_MP_STATE_UNINITIALIZED};
    return kvm_set_mpstate(kvm, vcpu, &mp_state);
}
#endif

#ifdef KVM_CAP_IRQCHIP
int kvm_get_irqchip(kvm_context_t, struct kvm_irqchip *chip);
int kvm_set_irqchip(kvm_context_t, struct kvm_irqchip *chip);
int kvm_get_lapic(kvm_context_t, int vcpu, struct kvm_lapic_state *s);
int kvm_set_lapic(kvm_context_t, int vcpu, struct kvm_lapic_state *s);
int kvm_inject_nmi(kvm_context_t, int vcpu);
#endif


#ifdef KVM_CAP_PIT
int kvm_get_pit(kvm_context_t kvm, struct kvm_pit_state *s);
int kvm_set_pit(kvm_context_t kvm, struct kvm_pit_state *s);
#endif

#ifdef KVM_CAP_VAPIC
int kvm_enable_tpr_access_reporting(kvm_context_t kvm, int vcpu);
int kvm_disable_tpr_access_reporting(kvm_context_t kvm, int vcpu);
int kvm_enable_vapic(kvm_context_t kvm, int vcpu, uint64_t vapic);
#endif

#endif
