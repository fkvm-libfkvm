#define FKVM_INTERNAL
#include <sys/fkvm.h>

#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <inttypes.h>
#include <assert.h>
#include <libdis.h>

#include "disasm.h"
#include "libkvm.h"
#include "libfkvm-common.h"

int
kvm_reg_from_x86_reg(int x86_reg_id)
{
	static const int const map[] = {
		-1,
		KVM_REG_RAX,
		KVM_REG_RCX,
		KVM_REG_RDX,
		KVM_REG_RBX,
		KVM_REG_RSP,
		KVM_REG_RBP,
		KVM_REG_RSI,
		KVM_REG_RDI,
		KVM_REG_RAX,
		KVM_REG_RCX,
		KVM_REG_RDX,
		KVM_REG_RBX,
		KVM_REG_RSP,
		KVM_REG_RBP,
		KVM_REG_RSI,
		KVM_REG_RDI,
		KVM_REG_RAX,
		KVM_REG_RCX,
		KVM_REG_RDX,
		KVM_REG_RBX,
		KVM_REG_RAX,
		KVM_REG_RCX,
		KVM_REG_RDX,
		KVM_REG_RBX,
		-1, /* mm0 */
		-1, /* mm1 */
		-1, /* mm2 */
		-1, /* mm3 */
		-1, /* mm4 */
		-1, /* mm5 */
		-1, /* mm6 */
		-1, /* mm7 */
		-1, /* xmm0 */
		-1, /* xmm1 */
		-1, /* xmm2 */
		-1, /* xmm3 */
		-1, /* xmm4 */
		-1, /* xmm5 */
		-1, /* xmm6 */
		-1, /* xmm7 */
		-1, /* dr0 */
		-1, /* dr1 */
		-1, /* dr2 */
		-1, /* dr3 */
		-1, /* dr4 */
		-1, /* dr5 */
		-1, /* dr6 */
		-1, /* dr7 */
		-1, /* cr0 */
		-1, /* cr1 */
		-1, /* cr2 */
		-1, /* cr3 */
		-1, /* cr4 */
		-1, /* cr5 */
		-1, /* cr6 */
		-1, /* cr7 */
		-1, /* tr0 */
		-1, /* tr1 */
		-1, /* tr2 */
		-1, /* tr3 */
		-1, /* tr4 */
		-1, /* tr5 */
		-1, /* tr6 */
		-1, /* tr7 */
		-1, /* es */
		-1, /* cs */
		-1, /* ss */
		-1, /* ds */

		-1, /* fs */
		-1, /* gs */
		-1, /* ldtr */
		-1, /* gdtr */
		-1, /* st(0) */
		-1, /* st(1) */
		-1, /* st(2) */
		-1, /* st(3) */
		-1, /* st(4) */
		-1, /* st(5) */
		-1, /* st(6) */
		-1, /* st(7) */
		-1, /* eflags, */
		-1, /* fpctrl, */
		-1, /* fpstat */
		-1, /* fptag, */
		-1, /* eip */
		-1, /* ip */
		-1, /* idtr */
		-1, /* mxcsr */
		-1, /* tr */
		-1, /* cs_msr */
		-1, /* esp_msr */
		-1, /* eip_msr */
	};

	if (x86_reg_id <= 0 || x86_reg_id >= ARRAY_SIZE(map))
		return -1;
	return map[x86_reg_id];
}

int
cr_num_from_x86_reg(int x86_reg_id)
{
	/* CR0: 49
	   CR1: 50
	   CR2: 51
	   CR3: 52
	   CR4: 53
	   CR5: 54
	   CR6: 55
	   CR7: 58 */
	int cr_num;
	cr_num = x86_reg_id - 49;
	if (cr_num >= 0 && cr_num <= 7)
		return cr_num;
	else
		return -1;
}

struct kvm_segment*
kvm_seg_from_x86_op(struct kvm_sregs *sregs, x86_op_t *op)
{
	switch (op->flags & 0xF00) {
		case op_es_seg: return &sregs->es;
		case op_cs_seg: return &sregs->cs;
		case op_ss_seg: return &sregs->ss;
		case op_ds_seg: return &sregs->ds;
		case op_fs_seg: return &sregs->fs;
		case op_gs_seg: return &sregs->gs;
		default:
			EXIT_ERR_PATH();
			return NULL;
	}

}

uint64_t
get_source_data(struct kvm_regs *regs, x86_op_t *op)
{
	uint64_t source;

	if (op->type == op_register) {
		source = kvm_regs_get(regs,
				      kvm_reg_from_x86_reg(op->data.reg.id));
	}
	else if (op->type == op_immediate) {
		source = op->data.qword;
	}
	else {
		EXIT();
	}

	//printf("source = %" PRIx64 "\n", source);

	source = mask_reg(source, x86_operand_size(op));
	return source;
}

uint64_t
get_memi_address(struct kvm_regs *regs, struct kvm_sregs *sregs,
                x86_op_t *op, size_t size)
{
	struct kvm_segment *segment;
	uint64_t address = 0;

	if (op->type == op_expression) {
		int reg_idx;
		x86_ea_t *ea;

		ea = &op->data.expression;

		if (ea->base.id != 0) {
			reg_idx = kvm_reg_from_x86_reg(ea->base.id);
			address += kvm_regs_get(regs, reg_idx);
		}

		if (ea->index.id != 0) {
			reg_idx = kvm_reg_from_x86_reg(ea->index.id);
			address += ea->scale * kvm_regs_get(regs, reg_idx);
		}

		if (ea->disp_size != 0 && ea->disp != 0) {
			address += ea->disp; /* TODO: disp_sign ? */
		}
	}
	else if (op->type == op_offset) {
		address = op->data.offset;
	}
	else {
		EXIT(); //TODO
	}

	//printf("address = %" PRIx64 "\n", address);
	address = mask_reg(address, size);

	switch (op->flags & ~0xFF) {
		case op_cs_seg:
			segment = &sregs->cs;
			break;
		case op_ds_seg:
			segment = &sregs->ds;
			break;
		case op_es_seg:
			segment = &sregs->es;
			break;
		case op_fs_seg:
			segment = &sregs->fs;
			break;
		case op_gs_seg:
			segment = &sregs->gs;
			break;
		case op_ss_seg:
			segment = &sregs->ss;
			break;
		default:
			assert(0);
			return -1;
	}

	/* if (reg_value > segment->limit) */

	//printf("segment base: %" PRIx64 "\n", segment->base);
	return segment->base + address;
}

unsigned int
get_x86_insn(const uint64_t insn_addr, x86_insn_t *insn)
{
	uint8_t buf[MAX_INSN_SIZE];
	unsigned int insn_size;

	cpu_virtual_memory_rw(insn_addr, buf, MAX_INSN_SIZE, 0);

	#if 1
	{
		int i;
		printf("buf @ 0x%" PRIx64 ":", insn_addr);
		for (i = 0; i < MAX_INSN_SIZE; i++)
			printf(" %02x", buf[i]);
		printf("\n");

	}
	#endif

	insn_size = x86_disasm(buf, MAX_INSN_SIZE, 0, 0, insn);
	if (insn_size == 0)
		return -1;

	#if 1
	{
		char line[80];
		x86_format_insn(insn, line, 80, intel_syntax);
		printf("%s\n", line);
	}
	#endif

	return insn_size;
}

void
libdisasm_init(size_t reg_size)
{
	if (reg_size == 2) {
		x86_init(opt_16_bit, NULL, NULL);
	}
	else if (reg_size == 4) {
		x86_init(opt_none, NULL, NULL);
	}
	else {
		EXIT();
	}
}

void
libdisasm_cleanup()
{
	if (!x86_cleanup())
		EXIT_ERR_PATH();
}
