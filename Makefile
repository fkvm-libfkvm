include config.mak
include config-$(ARCH).mak

# cc-option
# Usage: OP_CFLAGS+=$(call cc-option, -falign-functions=0, -malign-functions=0)
cc-option = $(shell if $(CC) $(1) -S -o /dev/null -xc /dev/null \
              > /dev/null 2>&1; then echo "$(1)"; else echo "$(2)"; fi ;)

CFLAGS += $(autodepend-flags) -g -O0 -fomit-frame-pointer -O -Wall -Werror
#CFLAGS += $(call cc-option, -fno-stack-protector, "")
#CFLAGS += $(call cc-option, -fno-stack-protector-all, "")
CFLAGS += -I $(LIBKVM_KERNELDIR)/include -I /usr/local/include

LDFLAGS += $(CFLAGS)

CXXFLAGS = $(autodepend-flags)

autodepend-flags = -MMD -MF $(dir $*).$(notdir $*).d


all: libkvm.a

libkvm.a: disasm.o ioio.o mmio.o cr.o libkvm.o
	$(AR) rcs $@ $^

-include .*.d

clean:
	$(RM) *.o *.a .*.d
